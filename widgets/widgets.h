#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include <QTreeWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

class TreeNode : public QTreeWidgetItem
{
public:
     QObject * obj;
};

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window(QWidget *parent = nullptr);
    ~Window();
    void displayBranch (QTreeWidgetItem * top, QObject * obj);
    void display();

    void displayLine (QString name, QString value);
    void displayTable (QObject * obj);

private slots:
    void on_tree_itemActivated(QTreeWidgetItem *item, int column);
    void on_tree_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);

private:
    Ui::Window *ui;
};
#endif // WINDOW_H
