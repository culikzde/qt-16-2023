#include "widgets.h"
#include "ui_widgets.h"
#include <QTableWidget>
#include <QMetaObject>
#include <QMetaProperty>
#include <QWindow>
#include <QApplication>

Window::Window(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Window)
{
    ui->setupUi(this);
    ui->vsplitter->setStretchFactor (0, 4);
    ui->vsplitter->setStretchFactor (1, 1);
    ui->tree->header()->hide();
    display ();
}

Window::~Window()
{
    delete ui;
}

void Window::displayBranch (QTreeWidgetItem * top, QObject * obj)
{
   TreeNode * node = new TreeNode;

   QString s = obj->objectName();
   if (QMainWindow * w = dynamic_cast <QMainWindow*> (obj))
       s = w->windowTitle ();

   s = s + " : " + obj->metaObject()->className();

   node->setText (0, s);
   node->obj = obj;

   for (QObject * t : obj->children())
       displayBranch (node, t);

   top->addChild (node);
}

void Window::display()
{
    displayBranch (ui->tree->invisibleRootItem(), this);
    /*
    for (QWindow * w : qApp->allWindows())
    {
       displayBranch (ui->tree->invisibleRootItem(), w);
    }
    */
}

void Window::displayLine (QString name, QString value)
{
    int inx = ui->table->rowCount ();
    ui->table->setRowCount (inx+1);

    QTableWidgetItem * node = new QTableWidgetItem;
    node->setText (name);
    ui->table->setItem (inx, 0, node);

    node = new QTableWidgetItem;
    node->setText (value);
    ui->table->setItem (inx, 1, node);
}

void Window::displayTable (QObject *obj)
{
    // ui->table->clear ();
    ui->table->setColumnCount (2);
    ui->table->setRowCount (0);
    // displayLine ("abc", "1");
    // displayLine ("def", "2");

    const QMetaObject  * cls = obj->metaObject ();
    int cnt = cls->propertyCount();
    for (int inx = 0; inx < cnt; inx++)
    {
        QMetaProperty prop = cls->property (inx);
        displayLine (prop.name (), prop.read (obj).toString ());
    }
}

void Window::on_tree_itemActivated(QTreeWidgetItem *node0, int column)
{
}

void Window::on_tree_currentItemChanged(QTreeWidgetItem *node0, QTreeWidgetItem *previous)
{
    // node0->setForeground (0, QColor (0, 255, 0));
    if (TreeNode * node = dynamic_cast <TreeNode*> (node0))
    {
        displayTable (node->obj);
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Window w;

    if (1)
    {
    QFont font = w.font();
    font. setPointSize (font.pointSize() + 4);
    w.setFont (font);
    }

    w.show();
    return a.exec();
}




