#include "draw.h"
#include "ui_draw.h"
#include "colorbutton.h"
#include "toolbutton.h"
#include "box.h"

#include <QApplication>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->vsplitter->setStretchFactor (0, 3);
    ui->vsplitter->setStretchFactor (1, 1);

    ui->hsplitter->setStretchFactor (0, 1);
    ui->hsplitter->setStretchFactor (1, 3);
    ui->hsplitter->setStretchFactor (2, 1);

    addToolButtons (ui->toolBar);
    addColorButtons (ui->toolBar);

    scene = new QGraphicsScene ();
    ui->graphicsView->setScene (scene);

    scene->addLine (0, 0, 100, 100, QColor ("red"));

    Box * box = new Box;
    /*
    QGraphicsRectItem * box = new QGraphicsRectItem;
    box->setPos (0, 0);
    box->setRect (0, 0, 200, 100);
    box->setPen (QColor ("blue"));
    box->setBrush (QColor ("yellow"));
    box->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
    box->setToolTip ("box");
    */
    scene->addItem (box);

    for (int i = 1; i <= 2; i++)
    {
        QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
        e->setPos (30 + (i-1) * 100, 30);
        e->setRect (0, 0, 40, 40);
        e->setPen (QColor ("blue"));
        e->setBrush (QColor ("cornflowerblue"));
        e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
        e->setParentItem (box);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

void MainWindow::on_quitMenu_triggered()
{
    close ();
}

