#ifndef PROPERTYTABLE_H
#define PROPERTYTABLE_H

#include <QTableWidget>

class PropertyTable : public QTableWidget
{
    Q_OBJECT
public:
    explicit PropertyTable (QWidget * parent = nullptr);
};

#endif // PROPERTYTABLE_H
