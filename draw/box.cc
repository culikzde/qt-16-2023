#include "box.h"
#include "port.h"
#include "toolbutton.h" // toolFormat

Box::Box()
{
    setRect (0, 0, 200, 100);
    setPen (QColor ("blue"));
    setBrush (QColor ("yellow"));
    setToolTip ("box");

    setFlag (QGraphicsItem::ItemIsMovable);
    setFlag (QGraphicsItem::ItemIsSelectable);
    #ifdef QT_4_6
        setFlag (QGraphicsItem::ItemSendsScenePositionChanges);
    #endif

    setAcceptDrops (true); /* <-- */
}

void Box::dragEnterEvent (QGraphicsSceneDragDropEvent *event)
{
    const QMimeData * mimeData = event->mimeData();
    if (mimeData->hasColor () || mimeData->hasFormat(toolFormat))
       event->setAccepted (true);
    else
       event->setAccepted (false);
}

void Box::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    const QMimeData * mimeData = event->mimeData();
    if (mimeData->hasColor ())
    {
        QColor color = mimeData->colorData().value<QColor>();
        if (event->dropAction() == Qt::CopyAction)
           setPen (color);
        else
           setBrush (color);
    }
    else if (mimeData->hasFormat (toolFormat))
    {
        QPointF pos = event->scenePos ();
        pos = this->mapFromScene (pos);

        QString name = mimeData->data (toolFormat);
        if (name == "rectangle")
        {
            QGraphicsRectItem * e = new QGraphicsRectItem;
            e->setRect (0, 0, 80, 40);
            e->setPen (QColor ("blue"));
            e->setBrush (QColor ("orange"));
            e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
            e->setPos (pos);
            e->setParentItem (this);
        }
        if (name == "ellipse")
        {
            QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
            e->setPos (pos);
            e->setRect (0, 0, 40, 40);
            e->setPen (QColor ("blue"));
            e->setBrush (QColor ("orange"));
            e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
            e->setParentItem (this);
        }
        if (name == "line")
        {
            QPointF pos2 = pos + QPointF (100, 100);

            Port * start = new Port;
            start->setBrush (QColor ("lime"));
            start->setPen (QColor ("green"));
            start->setPos (pos);
            start->setParentItem (this);

            Port * stop = new Port;
            stop->setBrush (QColor ("yellow"));
            stop->setPen (QColor ("green"));
            stop->setPos (pos2);
            stop->setParentItem (this);

            QGraphicsLineItem * e = new QGraphicsLineItem;
            e->setPen (QColor ("red"));
            e->setLine (QLineF (pos, pos2));
            e->setParentItem (this);

            start->line = e;
            start->start = true;

            stop->line = e;
            stop->start = false;
        }
    }

}
