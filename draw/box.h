#ifndef BOX_H
#define BOX_H

#include <QColor>
#include <QPen>
#include <QBrush>
#include <QMimeData>
#include <QGraphicsRectItem>
#include <QGraphicsSceneDragDropEvent>

class Box : public QGraphicsRectItem
{
public:
    Box();

    // QGraphicsItem interface
protected:
    void dragEnterEvent (QGraphicsSceneDragDropEvent *event);
    void dropEvent (QGraphicsSceneDragDropEvent *event);
};

#endif // BOX_H
