#include "toolbutton.h"

ToolButton::ToolButton (QString p_name, QString p_icon_name, QString p_format) :
    name (p_name)
{
   if (p_icon_name == "")
      p_icon_name = ":/icons/" + p_name + ".svg";
   icon = QIcon (p_icon_name);

   format = p_format;
   if (format == "")
      format = toolFormat;

   setIcon (icon);
   setText (name);
   setToolTip (name);
}

void ToolButton::mousePressEvent (QMouseEvent * event)
{
   if (event->button() == Qt::LeftButton )
   {
      QMimeData * mimeData = new QMimeData;
      mimeData->setData (format, name.toLatin1());

      QDrag * drag = new QDrag (this);
      drag->setMimeData (mimeData);
      drag->setPixmap (icon.pixmap (24, 24));
      drag->setHotSpot (QPoint (-16, -16));

      Qt::DropAction dropAction = drag->exec (Qt::MoveAction | Qt::CopyAction | Qt::LinkAction);
   }
}

/* ---------------------------------------------------------------------- */

void addToolButton (QToolBar * page, QString name, QString icon_name, QString format)
{
    ToolButton * b = new ToolButton (name, icon_name, format);
    page->addWidget (b);
}

void addToolButtons (QToolBar * page)
{
    addToolButton (page, "rectangle");
    addToolButton (page, "ellipse");
    addToolButton (page, "line");
}
