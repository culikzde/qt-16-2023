#ifndef TOOLBUTTON_H
#define TOOLBUTTON_H

#include <QToolButton>
#include <QToolBar>

#include <QMimeData>
#include <QDrag>
#include <QMouseEvent>

const QString toolFormat = "application/x-tool";

class ToolButton : public QToolButton
{
public:
    ToolButton();
private:
   QString name;
   QIcon icon;
   QString format;
protected:
    void mousePressEvent (QMouseEvent *event);
public:
    ToolButton (QString p_name, QString p_icon_name = "", QString p_format = "");
};

void addToolButton (QToolBar * page, QString name, QString icon_name = "", QString format = "");
void addToolButtons (QToolBar * page);

#endif // TOOLBUTTON_H
