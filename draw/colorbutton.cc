#include "colorbutton.h"

#include <QPainter>

/* ---------------------------------------------------------------------- */

ColorButton::ColorButton(QColor p_color, QString p_name) :
    color (p_color),
    name (p_name)
{
   setIcon (getPixmap ());
   setToolTip (name);
}

QPixmap ColorButton::getPixmap ()
{
   QPixmap pixmap (12, 12);
   pixmap.fill (Qt::transparent);

   QPainter painter (&pixmap);
   painter.setPen (Qt::NoPen);
   painter.setBrush (QBrush (color));
   painter.drawEllipse (0, 0, 12, 12);
   painter.end ();

   return pixmap;
}

void ColorButton::mousePressEvent (QMouseEvent * event)
{
   if (event->button() == Qt::LeftButton )
   {
      QMimeData * mimeData = new QMimeData;
      mimeData->setColorData (color);
      mimeData->setText (name);

      QDrag * drag = new QDrag (this);
      drag->setMimeData (mimeData);
      drag->setPixmap (getPixmap ());
      drag->setHotSpot (QPoint (-16, -16));

      Qt::DropAction dropAction = drag->exec (Qt::MoveAction | Qt::CopyAction | Qt::LinkAction);
   }
}

/* ---------------------------------------------------------------------- */

void addColorButton (QToolBar * page, QString name)
{
    ColorButton * b = new ColorButton (QColor (name), name);
    page->addWidget (b);
}

void addColorButtons (QToolBar * page)
{
    addColorButton (page, "red");
    addColorButton (page, "blue");
    addColorButton (page, "green");
    addColorButton (page, "yellow");
    addColorButton (page, "orange");
    addColorButton (page, "silver");
    addColorButton (page, "gold");
    addColorButton (page, "goldenrod");
    addColorButton (page, "lime");
    addColorButton (page, "lime green");
    addColorButton (page, "yellow green");
    addColorButton (page, "green yellow");
    addColorButton (page, "forest green");
    addColorButton (page, "coral");
    addColorButton (page, "cornflower blue");
    addColorButton (page, "dodger blue");
    addColorButton (page, "royal blue");
    addColorButton (page, "wheat");
    addColorButton (page, "chocolate");
    addColorButton (page, "peru");
    addColorButton (page, "sienna");
    addColorButton (page, "brown");
}

/* ---------------------------------------------------------------------- */
