#include "port.h"
#include <QColor>
#include <QBrush>

Port::Port()
{
    setRect (0, 0, 8, 8);
    setFlags (QGraphicsItem::ItemIsMovable |
              QGraphicsItem::ItemIsSelectable |
              QGraphicsItem::ItemSendsGeometryChanges // |
              // QGraphicsItem::ItemSendsScenePositionChanges
              );
}

QVariant Port::itemChange (GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemPositionChange)
    {
        if (line != nullptr)
            setBrush (QColor ("blue"));
    }
   return QGraphicsItem::itemChange (change, value);
}
