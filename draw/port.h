#ifndef PORT_H
#define PORT_H

#include <QGraphicsRectItem>
#include <QGraphicsLineItem>

class Port : public QGraphicsRectItem
{
public:
    Port ();

    QGraphicsLineItem * line = nullptr;
    bool start = true;


protected:
    QVariant itemChange (GraphicsItemChange change, const QVariant &value);
};

#endif // PORT_H
