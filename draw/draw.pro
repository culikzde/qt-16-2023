QT += core gui widgets

CONFIG += c++17

SOURCES += colorbutton.cc toolbutton.cc box.cc port.cc draw.cc

HEADERS += colorbutton.h  toolbutton.h box.h port.h draw.h

FORMS += draw.ui

RESOURCES += resources.qrc
