#ifndef TOOLBUTTON_H
#define TOOLBUTTON_H

#include <QToolButton>
#include <QToolBar>
#include <QIcon>

const QString toolFormat = "application/x-tool";

class ToolButton : public QToolButton
{
private:
   QString name;
   QIcon icon;
protected:
    void mousePressEvent (QMouseEvent *event);
public:
    ToolButton (QToolBar * parent, QString p_name, QString p_icon_name = "");
};

void addToolButtons (QToolBar * page);

#endif // TOOLBUTTON_H
