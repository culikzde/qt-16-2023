#include "mylabel.h"
#include <QMouseEvent>
#include <QPixmap>
#include <QPainter>

MyLabel::MyLabel (QWidget *parent)
    : QLabel (parent)
{

}

void MyLabel::mousePressEvent (QMouseEvent *event)
{
   start = event->pos();
}

void MyLabel::mouseReleaseEvent(QMouseEvent *event)
{
    QPoint stop = event->pos();

    QPixmap pix = pixmap (Qt::ReturnByValue);

    QPen pen (QColor ("red"));
    pen.setWidth (3);

    QPainter painter (&pix);
    painter.setPen (pen);
    painter.drawLine (start, stop);

    setPixmap (pix);
}
