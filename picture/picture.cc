#include "picture.h"
#include "ui_picture.h"
#include <QFileDialog>
#include <QApplication>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    openFile (QDir::homePath() + "/Downloads/Tulips.jpg");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFile (QString fileName)
{
    QPixmap pixmap (fileName);
    ui->label->setPixmap (pixmap);
    ui->label->setScaledContents (true);
}

void MainWindow::saveFile (QString fileName)
{
    ui->label->pixmap (Qt::ReturnByValue).save (fileName);
}

void MainWindow::on_openMenu_triggered()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open picture");
    if (fileName != "")
        openFile (fileName);
}

void MainWindow::on_saveMenu_triggered()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save picture");
    if (fileName != "")
        saveFile (fileName);
}

void MainWindow::on_runMenu_triggered()
{
    QPixmap pixmap = ui->label->pixmap (Qt::ReturnByValue);
    QImage image0 = pixmap.toImage();

    QImage image (image0.width(), image0.height(), QImage::Format_RGB32);
    image = image0;

    // for (int i = 0; i < 100; i++)
    //     image.setPixelColor (i, i, QColor ("orange"));

    uint * data = (uint *) image.bits();
    int w = image.width();
    for (int i = 0; i < 100; i++)
       data [i*(w+1)] = 0xff0000;

    pixmap.convertFromImage (image);
    ui->label->setPixmap (pixmap);
}



void MainWindow::on_quitMenu_triggered()
{
    close ();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

