#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void openFile (QString fileName);
    void saveFile (QString fileName);

private slots:
    void on_openMenu_triggered();

    void on_saveMenu_triggered();

    void on_quitMenu_triggered();

    void on_runMenu_triggered();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
