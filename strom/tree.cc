#include "tree.h"
#include "ui_tree.h"
#include <QTreeWidget>
#include <QDir>
#include <QDateTime>
#include <QApplication>

Window::Window(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Window)
{
    ui->setupUi(this);
    ui->treeWidget->setHeaderLabels (QStringList () << "name" << "size" << "date");
    displayDir ("/");
}

Window::~Window()
{
    delete ui;
}

void Window::displayDirectory (TreeNode * top, QString path, int depth)
{
    QDir dir (path);
    for (QFileInfo info : dir.entryInfoList (QDir::AllEntries | QDir::NoDotAndDotDot))
    {
        TreeNode * node = new TreeNode;
        node->setText (0, info.fileName());
        node->setToolTip (0, info.filePath());
        node->path = info.filePath();
        if (info.isDir())
        {
           node->setForeground  (0, QColor (255, 0, 0));
           if (depth > 1)
              displayDirectory (node, info.filePath(), depth-1);
        }
        else
        {
            node->setForeground  (0, QColor (0, 0, 255));
            node->setText (1, QString::number (info.size ()));
            node->setText (2, info.lastModified().toString ("yyyy-MM-dd HH:mm"));
        }
        top->addChild (node);
    }
    top->ready = true;
}

void Window::displayDir (QString path)
{
    QDir dir0 (path);
    QDir dir (dir0.absolutePath());

    QString s = dir.absolutePath();

    TreeNode * top = new TreeNode;
    top->setText (0, dir.dirName());
    top->setToolTip (0, s);
    top->setForeground  (0, QColor ("red"));
    top->path = s;
    ui->treeWidget->addTopLevelItem (top);

    displayDirectory (top, s, 1);

    ui->treeWidget->expandItem (top);
}

void Window::on_treeWidget_itemExpanded (QTreeWidgetItem * node0)
{
    int cnt = node0->childCount ();
    for (int i = 0; i < cnt ; i++)
    {
        QTreeWidgetItem * node1 = node0->child (i);
        if (TreeNode * node = dynamic_cast <TreeNode*> (node1))
        {
            if (! node->ready)
            {
                // node->setForeground (0, QColor (0, 255, 0));
                // setWindowTitle ("added " + node->text (0));
                displayDirectory (node, node->path, 1);
            }
        }
    }

}

void Window::on_pushButton_clicked()
{
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Window w;
    w.show();
    return a.exec();
}


