#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include <QTreeWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

class TreeNode : public QTreeWidgetItem
{
public:
     QString path;
     bool ready = false;
};


class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window(QWidget *parent = nullptr);
    ~Window();

    void displayDirectory (TreeNode * top, QString path, int depth);
    void displayDir (QString path);


private slots:
    void on_pushButton_clicked();

    void on_treeWidget_itemExpanded(QTreeWidgetItem *item);

private:
    Ui::Window *ui;
};
#endif // WINDOW_H
