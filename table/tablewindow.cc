#include "tablewindow.h"
#include "ui_tablewindow.h"
#include <QApplication>

TableWindow::TableWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::TableWindow),
    data (N, Vec (N, 0))
{
    ui->setupUi(this);

    // data = Mat (N, Vec (N, 1));
    for (int i = 0; i < N; i++)
         data [i][i] = 1;

    displayData (ui->table);
    edit_level = 1;
}

TableWindow::~TableWindow()
{
    delete ui;
}

void TableWindow::on_table_cellChanged(int line, int column)
{
    if (edit_level > 0)
    {
        edit_level --;

        QTableWidgetItem * node = ui->table->item (line, column);
        QString s = node->text ();
        s = s.simplified ();
        bool ok;
        double value = s.toDouble (&ok);
        if (ok)
        {
           data [line][column] = value;
           node->setForeground (QColor ("green"));
        }
        else
        {
           node->setForeground (QColor ("red"));
        }

        edit_level ++;
    }
}

void TableWindow::on_actionRun_triggered()
{
    QTableWidget * t = new QTableWidget;
    ui->tabs->addTab (t, "Copy");
    ui->tabs->setCurrentWidget (t);
    displayData (t);
}

void TableWindow::displayData (QTableWidget * t)
{
    t->setRowCount (N);
    t->setColumnCount (N);

    for (int i = 0; i < N; i++)
        for (int k = 0; k < N; k++)
        {
            double value = data [i][k];
            QTableWidgetItem * node = new QTableWidgetItem;
            node->setText (QString::number (value));
            if (i == k)
            {
                node->setForeground (QColor ("orange"));
                node->setBackground (QColor ("yellow").light(180));
            }
            else
            {
               node->setForeground (QColor ("blue"));
            }
            node->setToolTip ("(" + QString::number(i) + "," + QString::number(k) + ")");
            t->setItem (i, k, node);
        }
}

void TableWindow::openFile (QString fileName)
{
    data = readMat (fileName.toStdString());
    displayData (ui->table);
}

void TableWindow::saveFile (QString fileName)
{
    writeMat (fileName.toStdString(), data);
}

void TableWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open file");
    if (fileName != "")
        openFile (fileName);
}


void TableWindow::on_actionSave_triggered()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save file");
    if (fileName != "")
       saveFile (fileName);
}

void TableWindow::on_actionQuit_triggered()
{
   close ();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TableWindow w;
    w.show();
    return a.exec();
}

void TableWindow::on_table_itemEntered (QTableWidgetItem *node)
{

}
