QT += core gui widgets

CONFIG += c++17 precompile_header

PRECOMPILED_HEADER += precompiled.h

SOURCES += lexer.cc tablewindow.cc

HEADERS += lexer.h tablewindow.h

FORMS += tablewindow.ui
