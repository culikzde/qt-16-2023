#ifndef TABLEWINDOW_H
#define TABLEWINDOW_H

#include <QTableWidget>
#include <QMainWindow>

#include "lexer.h"

// #include <vector>
using namespace std;

// typedef vector <double> Vec;
// typedef vector <Vec> Mat;

const int N = 5;

QT_BEGIN_NAMESPACE
namespace Ui { class TableWindow; }
QT_END_NAMESPACE


class TableWindow : public QMainWindow
{
    Q_OBJECT

public:
    TableWindow(QWidget *parent = nullptr);
    ~TableWindow();
    void openFile (QString fileName);
    void saveFile (QString fileName);

private:
    void displayData (QTableWidget * t);

private slots:
    void on_table_itemEntered(QTableWidgetItem *item);

    void on_table_cellChanged(int row, int column);

    void on_actionQuit_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionRun_triggered();

private:
    Ui::TableWindow *ui;
    int edit_level = 0;
    Mat data;
};
#endif // TABLEWINDOW_H
